<?php

namespace dautkom\routeros;


/**
 * RouterOS PHP API class v1.7
 * Author: Denis Basta
 *
 * Contributors:
 *    Nick Barnes
 *    Ben Menking (ben [at] infotechsc [dot] com)
 *    Jeremy Jefferson (http://jeremyj.com)
 *    Cristian Deluxe (djcristiandeluxe [at] gmail [dot] com)
 *    Mikhail Moskalev (mmv.rus [at] gmail [dot] com)
 *    Oļegs Čapligins (shader [at] dautkom [dot] lv)
 *
 * http://www.mikrotik.com
 * https://bitbucket.org/dautkom/php.routeros/wiki/Home
 *
 * @package dautkom\routeros
 */
class RouterosAPI
{

    /**
     * Show debug information
     * @var bool
     */
    public $debug = false;

    /**
     * Port to connect to (default 8729 for ssl)
     * @var int
     */
    public $port = 8728;

    /**
     * Connect using SSL (must enable api-ssl in IP/Services)
     * @var bool
     */
    public $ssl = false;

    /**
     * Connection attempt timeout and data read timeout in seconds
     * @var int
     */
    public $timeout = 3;

    /**
     * Connection attempt count
     * @var int
     */
    public $attempts = 5;

    /**
     * Delay between connection attempts in seconds
     * @var int
     */
    public $delay = 3;

    /**
     * Variable for storing connection error number, if any
     * @var int
     */
    public $errno;

    /**
     * Variable for storing connection error text, if any
     * @var string
     */
    public $errstr;

    /**
     * Connection state
     * @var bool
     */
    private $connected = false;

    /**
     * Variable for storing socket resource
     * @var resource
     */
    private $socket;


    /**
     * @ignore
     */
    public function __destruct()
    {
        $this->disconnect();
    }


    /**
     * Login to RouterOS
     *
     * @param string      $ip         Hostname (IP or domain) of the RouterOS server
     * @param string      $login      The RouterOS username
     * @param string      $password   The RouterOS password
     * @return bool                   If we are connected or not
     */
    public function connect(string $ip, string $login, string $password): bool
    {

        for ($attempt = 1; $attempt <= $this->attempts; $attempt++) {

            $this->connected = false;
            $protocol        = ($this->ssl ? 'ssl://' : '' );

            $this->debug("Connection attempt #$attempt to {$protocol}{$ip}:{$this->port}...");
            $this->socket = @fsockopen($protocol.$ip, $this->port, $this->errno, $this->errstr, $this->timeout);

            if ($this->socket) {

                socket_set_timeout($this->socket, $this->timeout);
                $this->write('/login');
                $response = $this->read(false);

                if (isset($response[0]) && isset($response[1]) && $response[0] == '!done') {

                    $matches = [];

                    if (preg_match_all('/[^=]+/i', $response[1], $matches)) {

                        if ($matches[0][0] == 'ret' && strlen($matches[0][1]) == 32) {

                            $this->write('/login', false);
                            $this->write("=name=$login", false);
                            $this->write('=response=00' . md5(chr(0) . $password . pack('H*', $matches[0][1])));

                            $response = $this->read(false);

                            if (isset($response[0]) && $response[0] == '!done') {
                                $this->connected = true;
                                break;
                            }

                        }

                    }

                }

                fclose($this->socket);

            }

            sleep($this->delay);

        }

        if ($this->connected) {
            $this->debug('Connected...');
        }
        else {
            $this->debug('Error...');
        }

        return $this->connected;

    }


    /**
     * Disconnect from RouterOS
     *
     * @return void
     */
    public function disconnect()
    {

        // let's make sure this socket is still valid.  it may have been closed by something else
        if( is_resource($this->socket) ) {
            fclose($this->socket);
        }

        if ($this->connected) {
            $this->connected = false;
            $this->debug('Disconnected...');
        }

    }


    /**
     * Parse response from Router OS
     *
     * @param  array      $response   Response data
     * @return array                  Array with parsed data
     */
    public function parseResponse(array $response): array
    {

        $parsed      = [];
        $current     = null;
        $singlevalue = null;

        if (is_array($response)) {

            foreach ($response as $x) {

                if (in_array($x, ['!fatal','!re','!trap'])) {
                    if ($x == '!re') {
                        $current =& $parsed[];
                    }
                    else {
                        $current =& $parsed[$x][];
                    }
                }
                elseif ($x != '!done') {

                    $matches = [];

                    if (preg_match_all('/[^=]+/i', $x, $matches)) {

                        if ($matches[0][0] == 'ret') {
                            $singlevalue = [$matches[0][1]];
                        }

                        $current[$matches[0][0]] = (isset($matches[0][1]) ? $matches[0][1] : '');

                    }

                }

            }

            if (empty($parsed) && !is_null($singlevalue)) {
                $parsed = $singlevalue;
            }


        }

        return $parsed;

    }


    /**
     * Read data from Router OS
     *
     * @param  bool     $parse      Parse the data? default: true
     * @return array                Array with parsed or unparsed data
     */
    public function read(bool $parse=true): array
    {

        $response     = [];
        $receiveddone = false;
        $_            = "";

        while (true) {

            // Read the first byte of input which gives us some or all of the length of the remaining reply.
            $byte   = ord(fread($this->socket, 1));

            /**
             * If the first bit is set then we need to remove the first four bits, shift left 8
             * and then read another byte in.
             * We repeat this for the second and third bits.
             * If the fourth bit is set, we need to remove anything left in the first byte
             * and then read in yet another byte.
             */
            if ($byte & 128) {

                if (($byte & 192) == 128) {
                    $length = (($byte & 63) << 8) + ord(fread($this->socket, 1));
                }
                else {

                    if (($byte & 224) == 192) {
                        $length = (($byte & 31) << 8) + ord(fread($this->socket, 1));
                        $length = ($length << 8) + ord(fread($this->socket, 1));
                    }
                    else {

                        if (($byte & 240) == 224) {
                            $length = (($byte & 15) << 8) + ord(fread($this->socket, 1));
                            $length = ($length << 8) + ord(fread($this->socket, 1));
                            $length = ($length << 8) + ord(fread($this->socket, 1));
                        }
                        else {
                            $length = ord(fread($this->socket, 1));
                            $length = ($length << 8) + ord(fread($this->socket, 1));
                            $length = ($length << 8) + ord(fread($this->socket, 1));
                            $length = ($length << 8) + ord(fread($this->socket, 1));
                        }

                    }

                }

            }
            else {
                $length = $byte;
            }

            // If we have got more characters to read, read them in.
            if ($length > 0) {

                $_      = "";
                $retlen = 0;

                while ($retlen < $length) {

                    $toread = $length - $retlen;
                    $_     .= fread($this->socket, $toread);
                    $retlen = strlen($_);
                }

                $response[] = $_;
                $this->debug('>>> [' . $retlen . '/' . $length . '] bytes read.');

            }

            // If we get a !done, make a note of it.
            if ($_ == "!done") {
                $receiveddone = true;
            }

            $status = socket_get_status($this->socket);

            if ($length > 0) {
                $this->debug('>>> [' . $length . ', ' . $status['unread_bytes'] . ']' . $_);
            }

            if ((!$this->connected && !$status['unread_bytes']) || ($this->connected && !$status['unread_bytes'] && $receiveddone)) {
                break;
            }

        }

        if ($parse) {
            $response = $this->parseResponse($response);
        }

        return $response;

    }


    /**
     * Write (send) data to Router OS
     *
     * @param string      $command    A string with the command to send
     * @param mixed       $param2     If we set an integer, the command will send this data as a "tag"
     *                                If we set it to boolean true, the funcion will send the comand and finish
     *                                If we set it to boolean false, the funcion will send the comand and wait for next command
     *                                Default: true
     * @return boolean                Return false if no command especified
     */
    public function write(string $command, $param2 = true): bool
    {

        if ($command) {

            $data = explode("\n", $command);

            foreach ($data as $com) {
                $com = trim($com);
                fwrite($this->socket, $this->encodeLength(strlen($com)) . $com);
                $this->debug('<<< [' . strlen($com) . '] ' . $com);
            }

            if (gettype($param2) == 'integer') {
                fwrite($this->socket, $this->encodeLength(strlen('.tag=' . $param2)) . '.tag=' . $param2 . chr(0));
                $this->debug('<<< [' . strlen('.tag=' . $param2) . '] .tag=' . $param2);
            }
            elseif (gettype($param2) == 'boolean') {
                fwrite($this->socket, ($param2 ? chr(0) : ''));
            }

            return true;

        }

        return false;

    }


    /**
     * Write (send) data to Router OS
     *
     * @param string      $com        A string with the command to send
     * @param array       $arr        An array with arguments or queries
     * @return array                  Array with parsed
     */
    public function comm($com, $arr=[])
    {

        $i     = 0;
        $count = count($arr);
        $this->write($com, !$arr);

        if ($this->is_iterable($arr)) {

            foreach ($arr as $k => $v) {

                switch ($k[0]) {
                    case "?":
                        $el = "$k=$v";
                    break;
                    case "~":
                        $el = "$k~$v";
                    break;
                    default:
                        $el = "=$k=$v";
                    break;
                }

                $last = ($i++ == $count - 1);
                $this->write($el, $last);

            }

        }

        return $this->read();

    }


    /**
     * @param  string $length
     * @return string
     */
    private function encodeLength($length)
    {

        if ($length < 0x80) {
            $length = chr($length);
        }

        elseif ($length < 0x4000) {
            $length |= 0x8000;
            $length = chr(($length >> 8) & 0xFF) . chr($length & 0xFF);
        }
        elseif ($length < 0x200000) {
            $length |= 0xC00000;
            $length = chr(($length >> 16) & 0xFF) . chr(($length >> 8) & 0xFF) . chr($length & 0xFF);
        }

        elseif ($length < 0x10000000) {
            $length |= 0xE0000000;
            $length = chr(($length >> 24) & 0xFF) . chr(($length >> 16) & 0xFF) . chr(($length >> 8) & 0xFF) . chr($length & 0xFF);
        }

        elseif ($length >= 0x10000000) {
            $length = chr(0xF0) . chr(($length >> 24) & 0xFF) . chr(($length >> 16) & 0xFF) . chr(($length >> 8) & 0xFF) . chr($length & 0xFF);
        }

        return $length;

    }


    /**
     * Check, can be var used in foreach
     *
     * @param  mixed $var
     * @return bool
     */
    private function is_iterable($var): bool
    {
        return $var !== null && (is_array($var) || $var instanceof \Traversable || $var instanceof \Iterator || $var instanceof \IteratorAggregate);
    }


    /**
     * Print text for debug purposes
     *
     * @param  string  $text    Text to print
     * @return void
     */
    private function debug(string $text)
    {
        echo ($this->debug) ? "$text\n" : null;
    }

}
