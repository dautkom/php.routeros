# PHP RouterOS API
Client API for RouterOS/Mikrotik

This class was originally written by Denis Basta and updated by several contributors.  It aims to give a simple interface to the RouterOS API in PHP.

This repository is a fork of official one by [Ben Menking](https://github.com/BenMenking/routeros-api). Fork made for composer integration, PSR-4 and PHP7 compatibility.

## Contributors (before forking on Bitbucket)
* Nick Barnes
* Ben Menking (ben [at] infotechsc [dot] com)
* Jeremy Jefferson (http://jeremyj.com)
* Cristian Deluxe (djcristiandeluxe [at] gmail [dot] com)
* Mikhail Moskalev (mmv.rus [at] gmail [dot] com)

## Requirements

* PHP 7.0 or higher

## License

[MIT License](http://opensource.org/licenses/MIT)